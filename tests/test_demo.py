import unittest
import sys

sys.path.append('..')

class test_code(unittest.TestCase):

    def test_demoFunc(self):

        from democode import demoFunction

        self.assertTrue(demoFunction(2), 4)
