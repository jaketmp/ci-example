.. CI Example documentation master file, created by
   sphinx-quickstart on Wed Nov 28 14:03:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CI Example's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. automodule:: democode
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
