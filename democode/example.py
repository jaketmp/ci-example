import numpy

def demoFunction(value):
    """
    Demo function, returns value squared.

    :param float value: Square this number
    :returns: Square of *value*
    :rtype: float
    """
    if not isinstance(value, int):
        raise TypeError()

    return value * value
