"""
democode provides an example of CI intergration into a python project.
"""

from .example import demoFunction

__version__ = '1.0.1'

__all__ =  ['demoFunction']
